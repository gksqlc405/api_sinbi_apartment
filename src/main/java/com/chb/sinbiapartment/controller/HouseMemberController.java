package com.chb.sinbiapartment.controller;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.manager.HouseMemberManagerService;
import com.chb.sinbiapartment.service.manager.ResidentManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "세대원 (관리자)")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class HouseMemberController {
    private final HouseMemberManagerService houseMemberManagerService;
    private final ResidentManagerService residentManagerService;

    @ApiOperation(value = "세대원 등록 (관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "residentId", value = "입주민 시퀀스",required = true)
    })
    @PostMapping("/member/{residentId}")
    public CommonResult setHouseMember(@PathVariable long residentId, @RequestBody @Valid HouseMemberRequest request) {
        Resident resident = residentManagerService.getResidentId(residentId);
        houseMemberManagerService.setHouseMember(request, resident);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "세대원 리스트 (사용자)")
    @GetMapping("/houseMember")
    public ListResult<HouseMemberListItem> getHouseMemberList() {

        return ResponseService.getListResult(houseMemberManagerService.getHouseMemberList(), true);
    }
}
