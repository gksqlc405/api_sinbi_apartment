package com.chb.sinbiapartment.controller;

import com.chb.sinbiapartment.enums.Category;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.user.MeetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "입주민 라운지")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/meeting")
public class MeetingController {
    private final MeetingService meetingService;

    @ApiOperation(value = "입주민 라운지 게시글 등록 (사용자) (관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", value = "카테고리",required = true)
    })
    @PostMapping("/new/meeting/{category}")
    public CommonResult setMeeting(@PathVariable Category category, @RequestBody @Valid MeetingRequest request) {
        meetingService.setMeeting(category, request);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "입주민 라운지 리스트 가저오기 (사용자)(관리자)")
    @GetMapping("/list")
    public ListResult<MeetingListItem> getMeetingList() {
        return ResponseService.getListResult(meetingService.getMeetingList(),true);
    }

    @ApiOperation(value = "입주민 라운지 카테고리 게시글 가저오기 (사용자)(관리자)")
    @GetMapping("/list/getClimbing/{categoryValue}")
    public ListResult<MeetingBulletinListItem> getCategories(@PathVariable Category categoryValue) {
        return ResponseService.getListResult(meetingService.getCategories(categoryValue),true);
    }

    @ApiOperation(value = "카테고리 게시글 디테일 (사용자)(관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "meetingId", value = "입주민 라운지 시퀀스",required = true)
    })
    @GetMapping("/{meetingId}")
    public SingleResult<MeetingBulletinDetails> getComplainDetail(@PathVariable long meetingId) {

        return ResponseService.getSingleResult(meetingService.getBulletinDetail(meetingId));
    }



}
