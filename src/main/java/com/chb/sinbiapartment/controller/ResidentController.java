package com.chb.sinbiapartment.controller;

import com.chb.sinbiapartment.enums.ResidentGroup;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.service.MemberDataService;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.manager.ResidentManagerService;
import com.chb.sinbiapartment.service.user.ResidentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "입주민관리 (관리자)")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/resident")
public class ResidentController {
    private final ResidentManagerService residentManagerService;
    private final ResidentService residentService;
    private final MemberDataService memberDataService;

    @ApiOperation(value = "입주민 정보 등록")
    @PostMapping("/new/{residentGroup}")
    public CommonResult setResident(@RequestBody @Valid ResidentRequest request,@PathVariable ResidentGroup residentGroup) {
        memberDataService.setMember(residentGroup, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "내정보 디테일 (사용자)(관리자)")
    @GetMapping("/resident")
    public SingleResult<ResidentDetail> getResidentDetail() {

        return ResponseService.getSingleResult(residentService.getResidentDetail());
    }

    @ApiOperation(value = "입주민 리스트 가저오기 (관리자)")
    @GetMapping("/all")
    public ListResult<ResidentList> residentListItem() {
        return ResponseService.getListResult(residentManagerService.residentListItem(), true);
    }


    @ApiOperation(value = "입주민 디테일 가저오기 (관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "입주민 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<ResidentDetailItem> getDetails(@PathVariable long id) {
        return ResponseService.getSingleResult(residentManagerService.getResidentDetails(id));
    }


}
