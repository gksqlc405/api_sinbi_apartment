package com.chb.sinbiapartment.controller;

import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.service.Common.ComplainCommentsService;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.manager.ResidentManagerService;
import com.chb.sinbiapartment.service.user.ComplainUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "민원창구 댓글")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/complain-comments")
public class ComplainCommentsController {
    private final ComplainCommentsService complainCommentsService;
    private final ResidentManagerService residentManagerService;
    private final ComplainUserService complainUserService;

    @ApiOperation(value = "민원창구 댓글 등록")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "민원창구 시퀀스",required = true)
    })
    @PostMapping("/comment/document-id/{id}")
    public CommonResult setComplainComments(@PathVariable long id,
            @RequestBody @Valid ComplainCommentRequest request) {
        Complain complain = complainUserService.getComplainId(id);
        complainCommentsService.setComplainComments(request, complain);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "민원창구 댓글 리스트 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainId", value = "민원창구 시퀀스",required = true)
    })
    @GetMapping("/list/{complainId}")
    public ListResult<ComplainCommentsListItem> getComplainList(@PathVariable long complainId) {
        return ResponseService.getListResult(complainCommentsService.getCommentList(complainId), true);
    }

}
