package com.chb.sinbiapartment.controller;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.model.CommonResult;
import com.chb.sinbiapartment.model.ListResult;
import com.chb.sinbiapartment.model.MyCarListItem;
import com.chb.sinbiapartment.model.MyCarRequest;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.user.MyCarService;
import com.chb.sinbiapartment.service.manager.ResidentManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "입주민 차량 정보")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/my-car")
public class MyCarController {
    private final MyCarService myCarService;
    private final ResidentManagerService residentManagerService;

    @ApiOperation(value = "입주민 차량 등록 (관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "residentId", value = "입주민 시퀀스",required = true)
    })
    @PostMapping("/new/{residentId}")
    public CommonResult setMyCar(@PathVariable long residentId, @RequestBody @Valid MyCarRequest request) {
        Resident resident = residentManagerService.getResidentId(residentId);
        myCarService.setMyCar(request, resident);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "우리집 차량 리스트 가저오기 (사용자)")
    @GetMapping("/list")
    public ListResult<MyCarListItem> getMyCarList() {
        return ResponseService.getListResult(myCarService.getMyCarList(), true);
    }
}
