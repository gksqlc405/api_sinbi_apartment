package com.chb.sinbiapartment.controller;


import com.chb.sinbiapartment.entity.News;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.service.Common.NewsCommentsService;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.manager.NewsManagerService;
import com.chb.sinbiapartment.service.manager.ResidentManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "아파트 소식 댓글")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/news-comments")
public class NewsCommentsController {
    private final NewsCommentsService newsCommentsService;
    private final ResidentManagerService residentManagerService;
    private final NewsManagerService newsManagerService;


    @ApiOperation(value = "아파트 소식 댓글등록")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "아파트소식 시퀀스",required = true)
    })
    @PostMapping("/resident/news/{id}")
    public CommonResult setNewsComments(@PathVariable long id, @RequestBody @Valid NewsCommentsRequest request) {
        News news = newsManagerService.getNewsId(id);
        newsCommentsService.setNewsComments(request, news);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "아파트소식 댓글 리스트 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "newsId", value = "아파트소식 시퀀스",required = true)
    })
    @GetMapping("/list/{newsId}")
    public ListResult<NewsCommentListItem> getComplainList(@PathVariable long newsId) {
        return ResponseService.getListResult(newsCommentsService.getNewsCommentList(newsId), true);
    }

}
