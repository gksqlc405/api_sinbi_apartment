package com.chb.sinbiapartment.controller;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.model.CommonResult;
import com.chb.sinbiapartment.model.ListResult;
import com.chb.sinbiapartment.model.VisitCarListItem;
import com.chb.sinbiapartment.model.VisitCarRequest;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.manager.ResidentManagerService;
import com.chb.sinbiapartment.service.user.VisitCarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "방문차량")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/visit-car")
public class VisitCarController {
    private final VisitCarService visitCarUserService;
    private final ResidentManagerService residentManagerService;


    @ApiOperation(value = "방문차량 등록(사용자)")
    @PostMapping("/new/visitCar")
    public CommonResult setVisitCar( @RequestBody @Valid VisitCarRequest request) {
        visitCarUserService.setVisitCar(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "방문차량 신청내역 조회 (사용자)(관리자)")
    @GetMapping("/list/visitCar")
    public ListResult<VisitCarListItem> getVisitCarList() {
        return ResponseService.getListResult(visitCarUserService.getVisitCarList(), true);
    }

    @ApiOperation(value = "방문차량 신청상태 수정 (관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "visitCarId", value = "방문차량 시퀀스",required = true)
    })
    @PutMapping("/update/visitCar/{visitCarId}")
    public CommonResult putVisitCar(@PathVariable long visitCarId, boolean isApproval) {
        visitCarUserService.putVisitCar(visitCarId, isApproval);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "방문차량 신청취소 (사용자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "visitCarId", value = "방문차량 시퀀스",required = true)
    })
    @DeleteMapping("/visitCar/{visitCarId}")
    public CommonResult delVisitCar(@PathVariable long visitCarId) {
        visitCarUserService.delVisitCar(visitCarId);
        return ResponseService.getSuccessResult();
    }
}
