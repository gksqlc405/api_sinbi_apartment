package com.chb.sinbiapartment.controller;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.enums.ServiceState;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.manager.ComplainManagerService;
import com.chb.sinbiapartment.service.manager.ResidentManagerService;
import com.chb.sinbiapartment.service.user.ComplainUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "민원창구")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/complain")
public class ComplainController {
    private final ComplainUserService complainUserService;
    private final ResidentManagerService residentManagerService;
    private final ComplainManagerService complainManagerService;

    @ApiOperation(value = "민원창구 등록 (사용자)")
    @PostMapping("/new/complain")
    public CommonResult setComplain(@RequestBody @Valid ComplainRequest request) {
        complainUserService.setComplain(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "민원창구 리스트 가저오기 (사용자)(관리자)")
    @GetMapping("/list")
    public ListResult<ComplainTitleList> getComplainList() {
        return ResponseService.getListResult(complainUserService.getComplainList(), true);
    }

    @ApiOperation(value = "민원창구 디테일 (사용자)(관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainId", value = "민원창구 시퀀스",required = true)
    })
    @GetMapping("/complain/{complainId}")
    public SingleResult<ComplainDetail> getComplainDetail(@PathVariable long complainId) {

        return ResponseService.getSingleResult(complainUserService.getComplainDetail(complainId));
    }


    @ApiOperation(value = "민원상태 수정하기 (관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainId", value = "민원창구 시퀀스",required = true)
    })
    @PutMapping("/put/{complainId}")
    public CommonResult putComplainState(@PathVariable long complainId, ServiceState serviceState) {
        complainManagerService.putComplainState(complainId, serviceState);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "민원창구 접수 리스트 가저오기 (사용자)(관리자)")
    @GetMapping("/register")
    public ListResult<ComplainStateListItem> getRegisterList() {
        return ResponseService.getListResult(complainUserService.getRegisterList(), true);
    }

    @ApiOperation(value = "민원창구 진행 리스트 가저오기 (사용자)(관리자)")
    @GetMapping("/progress")
    public ListResult<ComplainStateListItem> getProgressList() {
        return ResponseService.getListResult(complainUserService.getProgressList(), true);
    }

    @ApiOperation(value = "민원창구 완료 리스트 가저오기 (사용자)(관리자)")
    @GetMapping("/completion")
    public ListResult<ComplainStateListItem> getCompletion() {
        return ResponseService.getListResult(complainUserService.getCompletion(), true);
    }
}
