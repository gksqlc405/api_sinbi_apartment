package com.chb.sinbiapartment.controller;

import com.chb.sinbiapartment.enums.ResidentGroup;
import com.chb.sinbiapartment.model.LoginRequest;
import com.chb.sinbiapartment.model.LoginResponse;
import com.chb.sinbiapartment.model.SingleResult;
import com.chb.sinbiapartment.service.LoginService;
import com.chb.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;

    @ApiOperation(value = "웹 - 관리자 로그인")
    @PostMapping("/web/admin")
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        LoginResponse test = loginService.doLogin(ResidentGroup.ROLE_ADMIN, loginRequest, "WEB");
        return ResponseService.getSingleResult(test);
    }

    @ApiOperation(value = "앱 - 일반유저 로그인")
    @PostMapping("/app/user")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(ResidentGroup.ROLE_USER, loginRequest, "APP"));
    }
}
