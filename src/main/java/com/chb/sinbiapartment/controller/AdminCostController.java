package com.chb.sinbiapartment.controller;


import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.manager.AdminCostManagerService;
import com.chb.sinbiapartment.service.manager.ResidentManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "관리비 ")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/manager")
public class AdminCostController {
    private final AdminCostManagerService adminCostService;
    private final ResidentManagerService residentManagerService;

    @ApiOperation(value = "관리비 등록 (관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "입주민 시퀀스",required = true)
    })
    @PostMapping("/new/{id}")
    public CommonResult setAdminCost(@PathVariable long id, @RequestBody @Valid AdminCostRequest request) {
        Resident resident = residentManagerService.getResidentId(id);
        adminCostService.setAdminCost(request, resident);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "관리비 월별 내역 (사용자)(관리자)")
    @GetMapping("/adminCost/year/{year}/month/{month}")
    public SingleResult<AdminCostDetail> getComplainDetail(@PathVariable int year, @PathVariable int month) {

        return ResponseService.getSingleResult(adminCostService.getAdminCostDetail(year,month));
    }

}
