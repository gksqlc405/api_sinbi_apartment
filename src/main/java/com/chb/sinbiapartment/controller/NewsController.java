package com.chb.sinbiapartment.controller;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.enums.NewsState;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.manager.ResidentManagerService;
import com.chb.sinbiapartment.service.manager.NewsManagerService;
import com.chb.sinbiapartment.service.user.NewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "아파트 소식 ")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/news")
public class NewsController {
    private final NewsManagerService newsManagerService;
    private final ResidentManagerService residentManagerService;

    private final NewsService newsService;

    @ApiOperation(value = "아파트 소식 등록 (관리자)")
    @PostMapping("/new/resident")
    public CommonResult setNews( @RequestBody @Valid NewsRequest request) {
        newsManagerService.setNews(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "아파트소식 디테일 (사용자)(관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "newsId", value = "아파트소식 시퀀스",required = true)
    })
    @GetMapping("/news/{newsId}")
    public SingleResult<NewsDetail> getNewsDetail(@PathVariable long newsId) {

        return ResponseService.getSingleResult(newsService.getNewsDetail(newsId));
    }


    @ApiOperation(value = "아파트소식 리스트 가저오기 (사용자)(관리자)")
    @GetMapping("/newsList")
    public ListResult<NewsTitleListItem> getNewsList() {

        return ResponseService.getListResult(newsService.getNewsTitleList(), true);
    }

    @ApiOperation(value = "아파트소식 예정리스트 가저오기 (사용자)(관리자)")
    @GetMapping("/schedule")
    public ListResult<NewsTitleListItem> getScheduleList() {

        return ResponseService.getListResult(newsService.getScheduleList(), true);
    }

    @ApiOperation(value = "아파트소식 진행리스트 가저오기 (사용자)(관리자)")
    @GetMapping("/progress")
    public ListResult<NewsTitleListItem> getProgress() {

        return ResponseService.getListResult(newsService.getProgress(), true);
    }

    @ApiOperation(value = "아파트소식 완료리스트 가저오기 (사용자)(관리자)")
    @GetMapping("/completion")
    public ListResult<NewsTitleListItem> getCompletion() {

        return ResponseService.getListResult(newsService.getCompletion(), true);
    }

    @ApiOperation(value = "아파트 소식 진행상태 수정 (관리자)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "newsId", value = "아파트소식 시퀀스",required = true)
    })
    @PutMapping("/put/{newsId}")
    public CommonResult putNewsState(@PathVariable long newsId, NewsState newsState) {
        newsService.putNewsState(newsId, newsState);
        return ResponseService.getSuccessResult();
    }
}
