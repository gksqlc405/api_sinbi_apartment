package com.chb.sinbiapartment.controller;

import com.chb.sinbiapartment.entity.Meeting;
import com.chb.sinbiapartment.entity.News;
import com.chb.sinbiapartment.entity.NewsComments;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.service.Common.MeetingCommentsService;
import com.chb.sinbiapartment.service.ResponseService;
import com.chb.sinbiapartment.service.user.MeetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Api(tags = "입주민 라운지 댓글")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/meeting-comment")
public class MeetingCommentController {
    private final MeetingCommentsService meetingCommentsService;
    private final MeetingService meetingService;

    @ApiOperation(value = "카테고리 게시글 댓글등록")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "meetingId", value = "아파트소식 시퀀스",required = true)
    })
    @PostMapping("/comment/{meetingId}")
    public CommonResult setMeetingComment(@PathVariable long meetingId, @RequestBody @Valid MeetingCommentRequest request) {
        Meeting meeting = meetingService.getMeetingId(meetingId);
        meetingCommentsService.setMeetingComment(request, meeting);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "카테고리 게시글 댓글 리스트 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "meetingId", value = "아파트소식 시퀀스",required = true)
    })
    @GetMapping("/getComment/{meetingId}")
    public ListResult<MeetingCommentListItem> getComplainList(@PathVariable long meetingId) {
        return ResponseService.getListResult(meetingCommentsService.getMeetingComment(meetingId), true);
    }
}
