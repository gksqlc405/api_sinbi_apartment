package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.AdminCostRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdminCost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long adminCostId;

    @ApiModelProperty(value = "입주민 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;
    @ApiModelProperty(value = "관리비", required = true)
    @Column(nullable = false)
    private Double price;
    @ApiModelProperty(value = "관리비 시작일", required = true)
    @Column(nullable = false)
    private LocalDate dateStart;
    @ApiModelProperty(value = "관리비 마지막일", required = true)
    @Column(nullable = false)
    private LocalDate dateEnd;


    private AdminCost(AdminCostBuilder builder) {
        this.resident = builder.resident;
        this.price = builder.price;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;


    }

    public static class AdminCostBuilder implements CommonModelBuilder<AdminCost> {
        private final Resident resident;
        private final Double price;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;

        public AdminCostBuilder(AdminCostRequest request, Resident resident) {
            this.resident = resident;
            this.price = request.getPrice();
            this.dateStart = LocalDate.now();
            this.dateEnd = LocalDate.now();
        }


        @Override
        public AdminCost build() {
            return new AdminCost(this);
        }
    }
}
