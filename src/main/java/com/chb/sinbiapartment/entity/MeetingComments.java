package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.MeetingCommentRequest;
import com.chb.sinbiapartment.model.NewsCommentsRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MeetingComments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "입주민 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @ApiModelProperty(value = "모임 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "meetingBulletinId", nullable = false)
    private Meeting meeting;

    @ApiModelProperty(value = "내용", required = true)
    @Column(nullable = false, length = 30)
    private String content;

    @ApiModelProperty(value = "등록일자", required = true)
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @ApiModelProperty(value = "수정일자", required = true)
    @Column(nullable = false)
    private LocalDateTime dateUpdate;


    private MeetingComments(MeetingCommentsBuilder builder) {
        this.resident = builder.resident;
        this.meeting = builder.meeting;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;

    }

    public static class MeetingCommentsBuilder implements CommonModelBuilder<MeetingComments> {

        private final Resident resident;
        private final Meeting meeting;
        private final String content;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MeetingCommentsBuilder(MeetingCommentRequest request, Resident resident, Meeting meeting) {
            this.resident = resident;
            this.meeting = meeting;
            this.content = request.getContent();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public MeetingComments build() {
            return new MeetingComments(this);
        }
    }
}
