package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.HouseMemberRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HouseMember {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long memberId;


    @ApiModelProperty(value = "입주민 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;


    @ApiModelProperty(value = "세대원 이름", required = true)
    @Column(nullable = false, length = 10)
    private String name;

    @ApiModelProperty(value = "세대원 연락", required = true)
    @Column(nullable = false, length = 13)
    private String phone;


    private HouseMember(HouseMemberBuilder builder) {
        this.name = builder.name;
        this.phone = builder.phone;
        this.resident = builder.resident;

    }

    public static class HouseMemberBuilder implements CommonModelBuilder<HouseMember> {
        private final String name;
        private final String phone;
        private final Resident resident;

        public HouseMemberBuilder(HouseMemberRequest request, Resident resident) {
            this.name = request.getName();
            this.phone = request.getPhone();
            this.resident = resident;
        }

        @Override
        public HouseMember build() {
            return new HouseMember(this);
        }
    }
}
