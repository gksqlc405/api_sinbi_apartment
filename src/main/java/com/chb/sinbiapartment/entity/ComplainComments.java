package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.ComplainCommentRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainComments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "입주민 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @ApiModelProperty(value = "민원창구 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complainId", nullable = false)
    private Complain complain;

    @ApiModelProperty(value = "내용", required = true)
    @Column(nullable = false, length = 50)
    private String content;

    @ApiModelProperty(value = "등록일자", required = true)
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @ApiModelProperty(value = "수정일자", required = true)
    @Column(nullable = false)
    private LocalDateTime dateUpdate;


    private ComplainComments(ComplainCommentsBuilder builder) {
        this.resident = builder.resident;
        this.complain = builder.complain;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;

    }

    public static class ComplainCommentsBuilder implements CommonModelBuilder<ComplainComments> {

        private final Resident resident;
        private final Complain complain;
        private final String content;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public ComplainCommentsBuilder(ComplainCommentRequest request, Resident resident, Complain complain) {
            this.resident = resident;
            this.complain = complain;
            this.content = request.getContent();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public ComplainComments build() {
            return new ComplainComments(this);
        }
    }

}
