package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.MyCarRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyCar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long myCarId;

    @ApiModelProperty(value = "입주민 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @ApiModelProperty(value = "차량번호", required = true)
    private String myCarNumber;

    @ApiModelProperty(value = "차량모델", required = true)
    private String carModel;

    @ApiModelProperty(value = "차량타입", required = true)
    private String carType;

    @ApiModelProperty(value = "등록일자", required = true)
    private LocalDate dateCreate;


    private MyCar(MyCarBuilder builder) {
        this.myCarNumber = builder.myCarNumber;
        this.carModel = builder.carModel;
        this.carType = builder.carType;
        this.dateCreate = builder.dateCreate;
        this.resident = builder.resident;

    }

    public static class MyCarBuilder implements CommonModelBuilder<MyCar> {

        private final String myCarNumber;

        private final String carModel;

        private final String carType;

        private final LocalDate dateCreate;

        private final Resident resident;

        public MyCarBuilder(MyCarRequest request, Resident resident) {
            this.myCarNumber = request.getMyCarNumber();
            this.carModel = request.getCarModel();
            this.carType = request.getCarType();
            this.dateCreate = LocalDate.now();
            this.resident = resident;
        }


        @Override
        public MyCar build() {
            return new MyCar(this);
        }
    }
}
