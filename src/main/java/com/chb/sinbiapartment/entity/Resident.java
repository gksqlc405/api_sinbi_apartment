package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.enums.ResidentGroup;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.ResidentRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Resident implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long residentId;
    @ApiModelProperty(value = "아이디", required = true)
    @Column(nullable = false, length = 20,unique = true)
    private String username;
    @ApiModelProperty(value = "비밀번호", required = true)
    @Column(nullable = false)
    private String password;

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private ResidentGroup residentGroup;
    @ApiModelProperty(value = "세대주 이름", required = true)
    @Column(nullable = false, length = 10)
    private String residentName;
    @ApiModelProperty(value = "주소", required = true)
    @Column(nullable = false, length = 50)
    private String address;
    @ApiModelProperty(value = "아파트 동 수", required = true)
    @Column(nullable = false, length = 10)
    private Integer aptDong;
    @ApiModelProperty(value = "아파트 호 수", required = true)
    @Column(nullable = false, length = 10)
    private Integer aptNumber;
    @ApiModelProperty(value = "연락처", required = true)
    @Column(nullable = false, length = 13)
    private String phone;
    @ApiModelProperty(value = "등록일자", required = true)
    @Column(nullable = false)
    private LocalDate dateCreate;

    @ApiModelProperty(value = "거주여부", required = true)
    @Column(nullable = false)
    private Boolean isEnable;


    private Resident(ResidentBuilder builder) {

        this.username = builder.username;
        this.password = builder.password;
        this.residentGroup = builder.residentGroup;
        this.residentName = builder.residentName;
        this.address = builder.address;
        this.aptDong = builder.aptDong;
        this.aptNumber = builder.aptNumber;
        this.phone = builder.phone;
        this.dateCreate = builder.dateCreate;
        this.isEnable = builder.isEnable;

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(residentGroup.toString()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnable;
    }

    public static class ResidentBuilder implements CommonModelBuilder<Resident> {
        private final String username;
        private final String password;
        private final ResidentGroup residentGroup;
        private final String residentName;
        private final String address;
        private final Integer aptDong;
        private final Integer aptNumber;
        private final String phone;
        private final LocalDate dateCreate;
        private final Boolean isEnable;


        public ResidentBuilder(ResidentGroup residentGroup ,ResidentRequest request) {
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.residentGroup = residentGroup;
            this.residentName = request.getResidentName();
            this.address = request.getAddress();
            this.aptDong = request.getAptDong();
            this.aptNumber = request.getAptNumber();
            this.phone =request.getPhone();
            this.dateCreate = LocalDate.now();
            this.isEnable = true;


        }

        @Override
        public Resident build() {
            return new Resident(this);
        }
    }


}
