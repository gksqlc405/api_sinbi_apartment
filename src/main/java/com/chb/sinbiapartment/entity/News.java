package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.enums.NewsState;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.NewsRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long newsId;

    @ApiModelProperty(value = "입주민 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;
    @ApiModelProperty(value = "제목", required = true)
    @Column(nullable = false,length = 30)
    private String title;
    @ApiModelProperty(value = "내용", required = true)
    @Column(nullable = false, length = 100)
    private String content;
    @ApiModelProperty(value = "진행상태", required = true)
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 11)
    private NewsState newsState;
    @ApiModelProperty(value = "등록일자", required = true)
    @Column(nullable = false)
    private LocalDateTime dateCreate;
    @ApiModelProperty(value = "수정일식", required = true)
    @Column(nullable = false)
    private LocalDateTime dateUpdate;


    public void putNewsState(NewsState newsState) {
        this.newsState = newsState;
    }

    private News(NewsBuilder builder) {
        this.resident = builder.resident;
        this.title = builder.title;
        this.content = builder.content;
        this.newsState = builder.newsState;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;


    }

    public static class NewsBuilder implements CommonModelBuilder<News> {
        private final Resident resident;
        private final String title;
        private final String content;
        private final NewsState newsState;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public NewsBuilder(NewsRequest request, Resident resident) {
            this.resident = resident;
            this.title = request.getTitle();
            this.content = request.getContent();
            this.newsState = NewsState.SCHEDULE;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public News build() {
            return new News(this);
        }
    }



}
