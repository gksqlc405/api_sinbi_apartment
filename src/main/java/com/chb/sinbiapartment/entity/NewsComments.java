package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.NewsCommentsRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NewsComments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "입주민 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @ApiModelProperty(value = "아파트소식 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "newsId", nullable = false)
    private News news;

    @ApiModelProperty(value = "내용", required = true)
    @Column(nullable = false, length = 50)
    private String content;

    @ApiModelProperty(value = "등록일자", required = true)
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @ApiModelProperty(value = "수정일자", required = true)
    @Column(nullable = false)
    private LocalDateTime dateUpdate;


    private NewsComments(NewsCommentsBuilder builder) {
        this.resident = builder.resident;
        this.news = builder.news;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;

    }

    public static class NewsCommentsBuilder implements CommonModelBuilder<NewsComments> {

        private final Resident resident;
        private final News news;
        private final String content;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public NewsCommentsBuilder(NewsCommentsRequest request, Resident resident, News news) {
            this.resident = resident;
            this.news = news;
            this.content = request.getContent();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public NewsComments build() {
            return new NewsComments(this);
        }
    }

}
