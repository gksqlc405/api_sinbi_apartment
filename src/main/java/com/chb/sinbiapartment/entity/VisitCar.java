package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.enums.ApprovalStatus;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.VisitCarApprovalStatusUpdateRequest;
import com.chb.sinbiapartment.model.VisitCarRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VisitCar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long visitCarId;

    @ApiModelProperty(value = "입주민 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;
    @ApiModelProperty(value = "차량번호", required = true)
    @Column(nullable = false, length = 20)
    private String carNumber;

    @ApiModelProperty(value = "처리상태", required = true)
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 17)
    private ApprovalStatus approvalStatus;
    @ApiModelProperty(value = "등록일자", required = true)
    @Column(nullable = false)
    private LocalDateTime dateCreate;
    @ApiModelProperty(value = "방문 시작일", required = true)
    @Column(nullable = false)
    private LocalDate dateStart;
    @ApiModelProperty(value = "방문 마지막일", required = true)
    @Column(nullable = false)
    private LocalDate dateEnd;

    @ApiModelProperty(value = "방문 승인여부", required = true)
    @Column(nullable = false)
    private Boolean isApproval;

    public void putApprovalStatus() {
        this.approvalStatus = ApprovalStatus.BEFORE_PROCESSING;
    }

    public void putProcessed() {
        this.approvalStatus = ApprovalStatus.PROCESSED;
    }

    private VisitCar(VisitCarBuilder builder) {
        this.resident = builder.resident;
        this.carNumber = builder.carNumber;
        this.approvalStatus = builder.approvalStatus;
        this.dateCreate = builder.dateCreate;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.isApproval = builder.isApproval;


    }

    public static class VisitCarBuilder implements CommonModelBuilder<VisitCar> {
        private final Resident resident;
        private final String carNumber;
        private final ApprovalStatus approvalStatus;
        private final LocalDateTime dateCreate;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;
        private final Boolean isApproval;

        public VisitCarBuilder(VisitCarRequest request, Resident resident) {
            this.resident = resident;
            this.carNumber = request.getCarNumber();
            this.approvalStatus = ApprovalStatus.BEFORE_PROCESSING;
            this.dateCreate = LocalDateTime.now();
            this.dateStart = LocalDate.now();
            this.dateEnd =  LocalDate.now();
            this.isApproval = false;
        }

        @Override
        public VisitCar build() {
            return new VisitCar(this);
        }
    }

}
