package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.enums.Category;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.MeetingRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Meeting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long meetingId;

    @ApiModelProperty(value = "입주민 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @ApiModelProperty(value = "카테고리", required = true)
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 20)
    private Category category;

    @ApiModelProperty(value = "게시글 제목", required = true)
    @Column(nullable = false, length = 30)
    private String title;

    @ApiModelProperty(value = "게시글 내용", required = true)
    @Column(nullable = false,length = 100)
    private String content;
    @ApiModelProperty(value = "등록일자", required = true)
    @Column(nullable = false)
    private LocalDate dateCreate;

    @ApiModelProperty(value = "수정일자", required = true)
    @Column(nullable = false)
    private LocalDate dateUpdate;




    private Meeting(MeetingBuilder builder) {
        this.resident = builder.resident;
        this.category = builder.category;
        this.title = builder.title;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;



    }

    public static class MeetingBuilder implements CommonModelBuilder<Meeting> {
        private final Resident resident;
        private final Category category;
        private final String title;
        private final String content;
        private final LocalDate dateCreate;
        private final LocalDate dateUpdate;

        public MeetingBuilder(Resident resident, Category category, MeetingRequest request) {
            this.resident = resident;
            this.category = category;
            this.title = request.getTitle();
            this.content = request.getContent();
            this.dateCreate = LocalDate.now();
            this.dateUpdate = LocalDate.now();
        }



        @Override
        public Meeting build() {
            return new Meeting(this);
        }
    }


}
