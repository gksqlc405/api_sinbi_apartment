package com.chb.sinbiapartment.entity;

import com.chb.sinbiapartment.enums.ServiceState;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.model.ComplainRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Complain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long complainId;

    @ApiModelProperty(value = "입주민 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;
    @ApiModelProperty(value = "제목", required = true)
    @Column(nullable = false, length = 30)
    private String title;
    @ApiModelProperty(value = "내용", required = true)
    @Column(nullable = false, length = 100)
    private String content;
    @ApiModelProperty(value = "민원 상태", required = true)
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 11)
    private ServiceState serviceState;
    @ApiModelProperty(value = "등록일자", required = true)
    @Column(nullable = false)
    private LocalDateTime dateCreate;
    @ApiModelProperty(value = "수정일자", required = true)
    @Column(nullable = false)
    private LocalDateTime dateUpdate;


    public void putComplainState(ServiceState serviceState) {
        this.serviceState = serviceState;
    }

    private Complain(ComplainBuilder builder) {
        this.resident = builder.resident;
        this.title = builder.title;
        this.content = builder.content;
        this.serviceState = builder.serviceState;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;


    }

    public static class ComplainBuilder implements CommonModelBuilder<Complain> {
        private final Resident resident;
        private final String title;
        private final String content;
        private final ServiceState serviceState;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public ComplainBuilder(ComplainRequest request, Resident resident) {
            this.resident = resident;
            this.title = request.getTitle();
            this.content = request.getContent();
            this.serviceState = ServiceState.REGISTER;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }


        @Override
        public Complain build() {
            return new Complain(this);
        }
    }
}
