package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import jdk.jfr.Name;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResidentLoginResponse {


    private Long residentId;

    private ResidentLoginResponse(ResidentLoginResponseBuilder builder) {
        this.residentId = builder.residentId;

    }

    public static class ResidentLoginResponseBuilder implements CommonModelBuilder<ResidentLoginResponse> {

        private final Long residentId;

        public ResidentLoginResponseBuilder(Resident resident) {
            this.residentId = resident.getResidentId();
        }

        @Override
        public ResidentLoginResponse build() {
            return new ResidentLoginResponse(this);
        }
    }
}
