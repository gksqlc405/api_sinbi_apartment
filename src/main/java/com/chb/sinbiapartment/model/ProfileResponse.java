package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import com.chb.sinbiapartment.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProfileResponse {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "회원그룹")
    private String residentGroup;

    @ApiModelProperty(notes = "회원그룹 한글명")
    private String residentGroupName;

    @ApiModelProperty(notes = "아이디")
    private String username;

    @ApiModelProperty(notes = "이름")
    private String name;

    @ApiModelProperty(notes = "가입일")
    private String dateCreate;

    private ProfileResponse(ProfileResponseBuilder builder) {
        this.id = builder.id;
        this.residentGroup = builder.residentGroup;
        this.residentGroupName = builder.residentGroupName;
        this.username = builder.username;
        this.name = builder.name;
        this.dateCreate = builder.dateCreate;
    }

    public static class ProfileResponseBuilder implements CommonModelBuilder<ProfileResponse> {
        private final Long id;
        private final String residentGroup;
        private final String residentGroupName;
        private final String username;
        private final String name;
        private final String dateCreate;

        public ProfileResponseBuilder(Resident resident) {
            this.id = resident.getResidentId();
            this.residentGroup = resident.getResidentGroup().toString();
            this.residentGroupName = resident.getResidentGroup().getName();
            this.username = resident.getUsername();
            this.name = resident.getResidentName();
            this.dateCreate = CommonFormat.convertLocalDateToString(resident.getDateCreate());
        }

        @Override
        public ProfileResponse build() {
            return new ProfileResponse(this);
        }
    }
}
