package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.enums.ApprovalStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VisitCarApprovalStatusUpdateRequest {
    private ApprovalStatus approvalStatus;
}
