package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainTitleList {
    private String title;
    private String serviceState;
    private Long complainId;


    private ComplainTitleList(ComplainTitleListBuilder builder) {
        this.title = builder.title;
        this.serviceState = builder.serviceState;
        this.complainId = builder.complainId;

    }

    public static class ComplainTitleListBuilder implements CommonModelBuilder<ComplainTitleList> {
        private final String title;
        private final String serviceState;
        private final Long complainId;

        public ComplainTitleListBuilder(Complain complain) {
            this.title = complain.getTitle();
            this.serviceState = complain.getServiceState().getName();
            this.complainId = complain.getComplainId();
        }



        @Override
        public ComplainTitleList build() {
            return new ComplainTitleList(this);
        }
    }
}
