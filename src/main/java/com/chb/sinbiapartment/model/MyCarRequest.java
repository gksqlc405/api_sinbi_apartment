package com.chb.sinbiapartment.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MyCarRequest {
    @NotNull
    @Length(min = 1, max = 20)
    private String myCarNumber;

    @NotNull
    @Length(min = 1, max = 30)
    private String carModel;

    @NotNull
    @Length(min = 1, max = 10)
    private  String carType;
}
