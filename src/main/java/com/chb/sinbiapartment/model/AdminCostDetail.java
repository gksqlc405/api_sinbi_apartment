package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.AdminCost;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdminCostDetail {

    private Long residentId;

    private LocalDate dateStart;

    private Double price;

    private AdminCostDetail(AdminCostDetailBuilder builder) {
        this.residentId = builder.residentId;
        this.dateStart = builder.dateStart;
        this.price = builder.price;

    }

    public static class AdminCostDetailBuilder implements CommonModelBuilder<AdminCostDetail> {
        private final Long residentId;

        private final LocalDate dateStart;

        private final Double price;

        public AdminCostDetailBuilder(AdminCost adminCost) {
            this.residentId = adminCost.getResident().getResidentId();
            this.dateStart = adminCost.getDateStart();
            this.price = adminCost.getPrice();
        }

        @Override
        public AdminCostDetail build() {
            return new AdminCostDetail(this);
        }
    }
}
