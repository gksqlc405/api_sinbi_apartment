package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Meeting;
import com.chb.sinbiapartment.enums.Category;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MeetingListItem {

    private String categoryValue;
    private String category;
    private String categoryTitle;
    private String categorySubTitle;

    private String image;

    private MeetingListItem(MeetingListItemBuilder builder) {

        this.categoryValue = builder.categoryValue;
        this.category = builder.category;
        this.categoryTitle = builder.categoryTitle;
        this.categorySubTitle = builder.categorySubTitle;
        this.image = builder.image;

    }

    public static class MeetingListItemBuilder implements CommonModelBuilder<MeetingListItem> {
        private final String categoryValue;
        private final String category;
        private final String categoryTitle;
        private final String categorySubTitle;
        private final String image;


        public MeetingListItemBuilder(Category category) {
            this.categoryValue = category.toString();
            this.category = category.getName();
            this.categoryTitle = category.getTitle();
            this.categorySubTitle = category.getSubTitle();
            this.image = category.getImage();
        }

        @Override
        public MeetingListItem build() {
            return new MeetingListItem(this);
        }
    }
}
