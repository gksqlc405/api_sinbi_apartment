package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.News;
import com.chb.sinbiapartment.enums.NewsState;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NewsTitleListItem {

    private Long newsId;

    private String title;

    private String newsState;


    private NewsTitleListItem(NewsTitleListItemBuilder builder) {
        this.newsId = builder.newsId;
        this.title = builder.title;
        this.newsState = builder.newsState;

    }

    public static class NewsTitleListItemBuilder implements CommonModelBuilder<NewsTitleListItem> {
        private final Long newsId;
        private final String title;
        private final String newsState;

        public NewsTitleListItemBuilder(News news) {
            this.newsId = news.getNewsId();
            this.title = news.getTitle();
            this.newsState = news.getNewsState().getName();
        }

        @Override
        public NewsTitleListItem build() {
            return new NewsTitleListItem(this);
        }
    }

}
