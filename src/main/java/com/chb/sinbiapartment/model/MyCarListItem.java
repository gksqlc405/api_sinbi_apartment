package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.MyCar;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyCarListItem {

    private Long residentId;

    private String myCarNumber;

    private String carModel;

    private String carType;

    private MyCarListItem(MyCarListItemBuilder builder) {
        this.residentId = builder.residentId;
        this.myCarNumber = builder.myCarNumber;
        this.carModel = builder.carModel;
        this.carType = builder.carType;

    }

    public static class MyCarListItemBuilder implements CommonModelBuilder<MyCarListItem> {
        private final Long residentId;

        private final String myCarNumber;

        private final String carModel;

        private final String carType;

        public MyCarListItemBuilder(MyCar myCar) {
            this.residentId = myCar.getResident().getResidentId();
            this.myCarNumber = myCar.getMyCarNumber();
            this.carModel = myCar.getCarModel();
            this.carType = myCar.getCarType();
        }


        @Override
        public MyCarListItem build() {
            return new MyCarListItem(this);
        }
    }
}
