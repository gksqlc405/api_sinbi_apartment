package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.HouseMember;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HouseMemberListItem {

    private Long residentId;

    private Long memberId;

    private String name;

    private HouseMemberListItem(HouseMemberDetailBuilder builder) {
        this.residentId = builder.residentId;
        this.memberId = builder.memberId;
        this.name = builder.name;

    }

    public static class HouseMemberDetailBuilder implements CommonModelBuilder<HouseMemberListItem> {
        private final Long residentId;

        private final Long memberId;

        private final String name;

        public HouseMemberDetailBuilder(HouseMember houseMember) {
            this.residentId = houseMember.getResident().getResidentId();
            this.memberId = houseMember.getMemberId();
            this.name = houseMember.getName();
        }


        @Override
        public HouseMemberListItem build() {
            return new HouseMemberListItem(this);
        }
    }
}
