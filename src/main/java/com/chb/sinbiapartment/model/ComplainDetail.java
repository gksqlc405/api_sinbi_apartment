package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
public class ComplainDetail {

    private Long id;
    private String title;
    private LocalDateTime dateCreate;
    private String content;
    private String userName;
    private Integer aptDong;



    private ComplainDetail(ComplainDetailBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.dateCreate = builder.dateCreate;
        this.content = builder.content;
        this.userName = builder.userName;
        this.aptDong = builder.aptDong;


    }

    public static class ComplainDetailBuilder implements CommonModelBuilder<ComplainDetail> {
        private final Long id;
        private final String title;
        private final LocalDateTime dateCreate;
        private final String content;
        private final String userName;
        private final Integer aptDong;


        public ComplainDetailBuilder(Complain complain) {
            this.id = complain.getComplainId();
            this.title = complain.getTitle();
            this.dateCreate = complain.getDateCreate();
            this.content = complain.getContent();
            this.userName = complain.getResident().getUsername();
            this.aptDong = complain.getResident().getAptDong();

        }

        @Override
        public ComplainDetail build() {
            return new ComplainDetail(this);
        }
    }
}
