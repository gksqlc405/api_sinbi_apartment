package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Meeting;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MeetingBulletinListItem {

    private Long meetingId;
    private String title;


    private MeetingBulletinListItem(MeetingBulletinListItemBuilder builder) {
        this.meetingId = builder.meetingId;
        this.title = builder.title;
    }

    public static class MeetingBulletinListItemBuilder implements CommonModelBuilder<MeetingBulletinListItem> {
        private final Long meetingId;
        private final String title;


        public MeetingBulletinListItemBuilder(Meeting meeting) {
            this.meetingId = meeting.getMeetingId();
            this.title = meeting.getTitle();

        }

        @Override
        public MeetingBulletinListItem build() {
            return new MeetingBulletinListItem(this);
        }
    }

}
