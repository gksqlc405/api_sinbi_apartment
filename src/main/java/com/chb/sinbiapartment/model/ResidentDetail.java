package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResidentDetail {

    private  Long residentId;
    private String residentName;

    private String phone;

    private String address;

    private Integer aptDong;

    private Integer aptNumber;

    private ResidentDetail(ResidentDetailBuilder builder) {
        this.residentId = builder.residentId;
        this.residentName = builder.residentName;
        this.phone = builder.phone;
        this.address = builder.address;
        this.aptDong = builder.aptDong;
        this.aptNumber = builder.aptNumber;

    }

    public static class ResidentDetailBuilder implements CommonModelBuilder<ResidentDetail> {
        private final   Long residentId;
        private final String residentName;

        private final String phone;

        private final String address;

        private final Integer aptDong;

        private final Integer aptNumber;

        public ResidentDetailBuilder(Resident resident) {
            this.residentId = resident.getResidentId();
            this.residentName = resident.getResidentName();
            this. phone = resident.getPhone();
            this.address = resident.getAddress();
            this.aptDong = resident.getAptDong();
            this.aptNumber = resident.getAptNumber();
        }


        @Override
        public ResidentDetail build() {
            return new ResidentDetail(this);
        }
    }
}
