package com.chb.sinbiapartment.model;
import com.chb.sinbiapartment.entity.MeetingComments;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MeetingCommentListItem {
    private Long meetingId;

    private String username;

    private String content;

    private LocalDateTime dateCreate;

    private String residentGroup;

    private MeetingCommentListItem(MeetingCommentListItemBuilder builder) {
        this.meetingId = builder.meetingId;
        this.username = builder.username;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.residentGroup = builder.residentGroup;

    }

    public static class MeetingCommentListItemBuilder implements CommonModelBuilder<MeetingCommentListItem> {
        private final Long meetingId;
        private final String username;

        private final String content;

        private final LocalDateTime dateCreate;

        private final String residentGroup;

        public MeetingCommentListItemBuilder(MeetingComments meetingComments) {
            this.meetingId = meetingComments.getMeeting().getMeetingId();
            this.username = meetingComments.getResident().getUsername();
            this.content = meetingComments.getContent();
            this.dateCreate = meetingComments.getDateCreate();
            this.residentGroup = meetingComments.getResident().getResidentGroup().getName();
        }

        @Override
        public MeetingCommentListItem build() {
            return new MeetingCommentListItem(this);
        }
    }
}

