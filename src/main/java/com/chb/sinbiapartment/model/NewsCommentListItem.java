package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.ComplainComments;
import com.chb.sinbiapartment.entity.NewsComments;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NewsCommentListItem {


    private String username;

    private String content;

    private LocalDateTime dateCreate;

    private String residentGroup;

    private NewsCommentListItem(NewsCommentListItemBuilder builder) {
        this.username = builder.username;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.residentGroup = builder.residentGroup;

    }

    public static class NewsCommentListItemBuilder implements CommonModelBuilder<NewsCommentListItem> {

        private final String username;

        private final String content;

        private final LocalDateTime dateCreate;

        private final String residentGroup;

        public NewsCommentListItemBuilder(NewsComments newsComments) {
            this.username = newsComments.getResident().getUsername();
            this.content = newsComments.getContent();
            this.dateCreate = newsComments.getDateCreate();
            this.residentGroup = newsComments.getResident().getResidentGroup().getName();
        }

        @Override
        public NewsCommentListItem build() {
            return new NewsCommentListItem(this);
        }
    }
}

