package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.ComplainComments;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.enums.ResidentGroup;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainCommentsListItem {

    private Long complainId;

    private String username;

    private String content;

    private LocalDateTime dateCreate;

    private String residentGroup;

    private ComplainCommentsListItem(ComplainCommentsListItemBuilder builder) {
        this.complainId = builder.complainId;
        this.username = builder.username;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.residentGroup = builder.residentGroup;

    }

    public static class ComplainCommentsListItemBuilder implements CommonModelBuilder<ComplainCommentsListItem> {
        private final Long complainId;
        private final String username;

        private final String content;

        private final LocalDateTime dateCreate;

        private final String residentGroup;

        public ComplainCommentsListItemBuilder(ComplainComments complainComments) {
            this.complainId = complainComments.getComplain().getComplainId();
            this.username = complainComments.getResident().getUsername();
            this.content = complainComments.getContent();
            this.dateCreate = complainComments.getDateCreate();
            this.residentGroup = complainComments.getResident().getResidentGroup().getName();
        }

        @Override
        public ComplainCommentsListItem build() {
            return new ComplainCommentsListItem(this);
        }
    }
}
