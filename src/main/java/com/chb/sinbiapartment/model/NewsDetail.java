package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.entity.News;
import com.chb.sinbiapartment.enums.ResidentGroup;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NewsDetail {


    private Long id;
    private String title;
    private LocalDateTime dateCreate;
    private String content;
    private String userName;
    private Integer aptDong;

    private String residentGroup;



    private NewsDetail(NewsDetailBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.dateCreate = builder.dateCreate;
        this.content = builder.content;
        this.userName = builder.userName;
        this.aptDong = builder.aptDong;
        this.residentGroup = builder.residentGroup;


    }

    public static class NewsDetailBuilder implements CommonModelBuilder<NewsDetail> {
        private final Long id;
        private final String title;
        private final LocalDateTime dateCreate;
        private final String content;
        private final String userName;
        private final Integer aptDong;
        private final String residentGroup;


        public NewsDetailBuilder(News news) {
            this.id = news.getNewsId();
            this.title = news.getTitle();
            this.dateCreate = news.getDateCreate();
            this.content = news.getContent();
            this.userName = news.getResident().getUsername();
            this.aptDong = news.getResident().getAptDong();
            this.residentGroup = news.getResident().getResidentGroup().getName();

        }

        @Override
        public NewsDetail build() {
            return new NewsDetail(this);
        }
    }
}


