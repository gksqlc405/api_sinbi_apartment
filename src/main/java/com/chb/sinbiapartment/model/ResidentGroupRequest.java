package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.enums.ResidentGroup;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ResidentGroupRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private ResidentGroup residentGroup;

}
