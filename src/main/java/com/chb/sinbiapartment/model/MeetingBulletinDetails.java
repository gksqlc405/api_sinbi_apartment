package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.entity.Meeting;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MeetingBulletinDetails {
    private Long meetingId;
    private String title;
    private LocalDate dateCreate;
    private String content;
    private String userName;
    private Integer aptDong;



    private MeetingBulletinDetails(MeetingBulletinDetailsBuilder builder) {
        this.meetingId = builder.meetingId;
        this.title = builder.title;
        this.dateCreate = builder.dateCreate;
        this.content = builder.content;
        this.userName = builder.userName;
        this.aptDong = builder.aptDong;


    }

    public static class MeetingBulletinDetailsBuilder implements CommonModelBuilder<MeetingBulletinDetails> {
        private final Long meetingId;
        private final String title;
        private final LocalDate dateCreate;
        private final String content;
        private final String userName;
        private final Integer aptDong;


        public MeetingBulletinDetailsBuilder(Meeting meeting) {
            this.meetingId = meeting.getMeetingId();
            this.title = meeting.getTitle();
            this.dateCreate = meeting.getDateCreate();
            this.content = meeting.getContent();
            this.userName = meeting.getResident().getUsername();
            this.aptDong = meeting.getResident().getAptDong();

        }

        @Override
        public MeetingBulletinDetails build() {
            return new MeetingBulletinDetails(this);
        }
    }
}
