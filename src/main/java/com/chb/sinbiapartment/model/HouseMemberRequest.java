package com.chb.sinbiapartment.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class HouseMemberRequest {
    @NotNull
    @Length(min = 1, max = 10)
    private String name;

    @NotNull
    @Length(min = 13, max = 13)
    private String phone;
}
