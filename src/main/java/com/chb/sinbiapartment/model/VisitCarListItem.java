package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.entity.VisitCar;
import com.chb.sinbiapartment.enums.ApprovalStatus;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VisitCarListItem {

    private Long visitCarId;
    private Long residentId;

    private String carNumber;

    private String approvalStatus;

    private LocalDateTime dateCreate;


    private VisitCarListItem(VisitCarListItemBuilder builder) {
        this.visitCarId = builder.visitCarId;
        this.residentId = builder.residentId;
        this.carNumber = builder.carNumber;
        this.approvalStatus = builder.approvalStatus;
        this.dateCreate = builder.dateCreate;
    }
    public static class VisitCarListItemBuilder implements CommonModelBuilder<VisitCarListItem> {
        private final Long visitCarId;
        private final Long residentId;

        private final String carNumber;

        private final String approvalStatus;

        private final LocalDateTime dateCreate;

        public VisitCarListItemBuilder(VisitCar visitCar) {
            this.visitCarId = visitCar.getVisitCarId();
            this.residentId = visitCar.getResident().getResidentId();
            this.carNumber = visitCar.getCarNumber();
            this.approvalStatus = visitCar.getApprovalStatus().getName();
            this.dateCreate = visitCar.getDateCreate();

        }


        @Override
        public VisitCarListItem build() {
            return new VisitCarListItem(this);
        }
    }
}
