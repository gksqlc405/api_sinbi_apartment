package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.enums.ResidentGroup;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class ResidentRequest {
    @NotNull
    @Length(min = 2, max = 15)
    private String username;

    @NotNull
    @Length(min = 2, max = 15)
    private String password;

    @NotNull
    @Length(min = 2, max = 15)
    private String passwordRe;

    @NotNull
    @Length(min = 1, max = 10)
    private String residentName;

    @NotNull
    @Length(min = 3, max = 50)
    private String address;

    @NotNull
    private Integer aptDong;

    @NotNull
    private Integer aptNumber;

    @NotNull
    @Length(min = 13, max = 13)
    private String phone;




}
