package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.enums.NewsState;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class NewsRequest {

    @NotNull
    @Length(min = 1, max = 30)
    private String title;
    @NotNull
    @Length(min = 1, max = 100)
    private String content;
}
