package com.chb.sinbiapartment.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminCostRequest {

    private Double price;
}
