package com.chb.sinbiapartment.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ComplainCommentRequest {

    @NotNull
    @Length(min = 1, max = 50)
    private String content;
}
