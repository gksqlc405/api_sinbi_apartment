package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainStateListItem {

    private Long complainId;

    private String serviceState;

    private String title;




    private ComplainStateListItem(ComplainRegisterListItemBuilder builder) {
        this.complainId = builder.complainId;
        this.serviceState = builder.serviceState;
        this.title = builder.title;

    }

    public static class ComplainRegisterListItemBuilder implements CommonModelBuilder<ComplainStateListItem> {
        private final Long complainId;

        private final String serviceState;

        private final String title;


        public ComplainRegisterListItemBuilder(Complain complain) {
            this.complainId = complain.getComplainId();
            this.serviceState = complain.getServiceState().getName();
            this.title = complain.getTitle();

        }

        @Override
        public ComplainStateListItem build() {
            return new ComplainStateListItem(this);
        }
    }


}
