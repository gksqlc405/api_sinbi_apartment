package com.chb.sinbiapartment.model;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResidentDetailItem {

    private Long id;
    private String username;
    private String residentName;
    private String address;
    private Integer aptDong;
    private Integer aptNumber;
    private String phone;


    private ResidentDetailItem(ResidentDetailItemBuilder builder) {
        this.id = builder.id;
        this.username = builder.username;
        this.residentName = builder.residentName;
        this.address = builder.address;
        this.aptDong = builder.aptDong;
        this.aptNumber = builder.aptNumber;
        this.phone = builder.phone;

    }


    public static class ResidentDetailItemBuilder implements CommonModelBuilder<ResidentDetailItem> {
        private final Long id;
        private final String username;
        private final String residentName;
        private final String address;
        private final Integer aptDong;
        private final Integer aptNumber;
        private final String phone;


        public ResidentDetailItemBuilder(Resident resident) {
            this.id = resident.getResidentId();
            this.username = resident.getUsername();
            this.residentName = resident.getResidentName();
            this.address = resident.getAddress();
            this.aptDong = resident.getAptDong();
            this.aptNumber = resident.getAptNumber();
            this.phone = resident.getPhone();
        }

        @Override
        public ResidentDetailItem build() {
            return new ResidentDetailItem(this);
        }
    }
}


