package com.chb.sinbiapartment.exception;

public class CNoUserNameException extends RuntimeException {
    public CNoUserNameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoUserNameException(String msg) {
        super(msg);
    }

    public CNoUserNameException() {
        super();
    }
}
