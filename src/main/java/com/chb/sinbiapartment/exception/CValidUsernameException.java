package com.chb.sinbiapartment.exception;

public class CValidUsernameException extends RuntimeException {
    public CValidUsernameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CValidUsernameException(String msg) {
        super(msg);
    }

    public CValidUsernameException() {
        super();
    }
}
