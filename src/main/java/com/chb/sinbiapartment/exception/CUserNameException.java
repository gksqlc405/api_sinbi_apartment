package com.chb.sinbiapartment.exception;

public class CUserNameException extends RuntimeException {
    public CUserNameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CUserNameException(String msg) {
        super(msg);
    }

    public CUserNameException() {
        super();
    }
}
