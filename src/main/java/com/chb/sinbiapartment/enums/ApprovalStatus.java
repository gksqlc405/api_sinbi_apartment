package com.chb.sinbiapartment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApprovalStatus {

    BEFORE_PROCESSING("처리전"),

    PROCESSED("처리완료");



    private final String name;
}
