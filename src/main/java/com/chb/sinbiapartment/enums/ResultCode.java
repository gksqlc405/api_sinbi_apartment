package com.chb.sinbiapartment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다.")
    , FAILED(-1, "실패하였습니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")
    , NO_USER_NAME(-50000, "존재하지 않는 아이디입니다.")
    , NO_Password(-10000, "비밀번호가 일치하지 않습니다")
    , USER_NAME(-20000, "이미 존재하는 아이디입니다.")
    , VALID_USERNAME(-2001, "유효한 아이디 형식이 아닙니다.")

    , ACCESS_DENIED(-1000, "권한이 없습니다.")
    , USERNAME_SIGN_IN_FAILED(-1001, "가입된 사용자가 아닙니다.")
    , AUTHENTICATION_ENTRY_POINT(-1002, "접근 권한이 없습니다.")
    ;

    private final Integer code;
    private final String msg;
}
