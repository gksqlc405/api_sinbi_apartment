package com.chb.sinbiapartment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NewsState {

    PROGRESS("진행"),
    COMPLETION("완료"),
    SCHEDULE("예정");

    private final String name;


}
