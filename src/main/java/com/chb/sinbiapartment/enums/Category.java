package com.chb.sinbiapartment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {


    HEALTH("헬스모임", "몸짱이 되고싶은자들!!", "같이 운동하며 서로 좋은 정보들 공유해요~!","health.png"),
    SOCCER("축구모임", "축구를 사랑하는 사람들", "축구를 하고싶은분들 모두 모이세요!!","soccer.png"),
    BASEBALL("야구모임","야구를 사랑하는 사람들", "야구를 하고싶은분들 모두모이세요!!","baseball.png"),
    CLIMBING("한사랑 산악회","인생은 오르락 내리락", "우리 같이 경치구경하고 건강도 같이 챙겨봐요!!","climbing.png"),
    FAMOUS_RESTOURANT("맛집모임","맛집탐방 공유해요^^","서로 맛집들을 공유해보세요~","food.png"),
    RESELL("중고거래", "현대 아파트에 중고마켓!!", "싸게싸게 중고물품 사고팔고~~","makect.png"),
    MOMCAFE("맘카페", "엄마들을 위한 공간!!", "아이들에게 좋은 정보들 공유해요^^","mom.png");





    private final String name;
    private final String title;
    private final String subTitle;
    private final String image;
}
