package com.chb.sinbiapartment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResidentGroup {

    ROLE_ADMIN("관리자"),
    ROLE_USER("입주민");

    private final String name;
}
