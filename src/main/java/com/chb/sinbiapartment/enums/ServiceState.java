package com.chb.sinbiapartment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ServiceState {


    REGISTER("접수"),
    PROGRESS("진행"),
    COMPLETION("완료");

    private final String name;
}
