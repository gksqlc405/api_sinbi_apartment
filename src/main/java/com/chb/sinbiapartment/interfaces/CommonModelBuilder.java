package com.chb.sinbiapartment.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
