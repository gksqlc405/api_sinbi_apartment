package com.chb.sinbiapartment.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;

    private static final String[] AUTH_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers(AUTH_WHITELIST);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManagerBean();
    }
 /*
    flutter 에서 header에 token 넣는법
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!; <-- 한줄 추가

    --- 아래 동일
     */

    // 퍼미션이 무엇인지 보기.
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authorizeRequests()
                        .antMatchers(HttpMethod.GET, "/exception/**").permitAll() // 전체 허용
                        .antMatchers("/v1/login/**").permitAll() // 전체허용
//                        .antMatchers("/v1/login/web/admin").hasAnyRole("ADMIN")
                        .antMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN")
                        .antMatchers("/v1/auth-test/test-user").hasAnyRole("USER")
                        .antMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/resident/new").hasAnyRole("ADMIN")
                        .antMatchers("/v1/member/houseMember").hasAnyRole("USER")
                        .antMatchers("/v1/resident/resident").hasAnyRole("USER")
                        .antMatchers("/v1/manager/adminCost/year/{year}/month/{month}").hasAnyRole("USER")
                        .antMatchers("/v1/my-car/list").hasAnyRole("USER")
                        .antMatchers("/v1/my-car/list").hasAnyRole("USER")
                        .antMatchers("/v1/complain/new/complain").hasAnyRole("USER")
                        .antMatchers("/v1/complain/list").hasAnyRole("USER", "ADMIN")
                        .antMatchers("/v1/complain/register").hasAnyRole("USER", "ADMIN")
                        .antMatchers("/v1/complain/progress").hasAnyRole("USER", "ADMIN")
                        .antMatchers("/v1/complain/completion").hasAnyRole("USER", "ADMIN")
                        .antMatchers("/v1/complain/put/{complainId}").hasAnyRole( "ADMIN")
                        .antMatchers("/v1/complain/complain/{complainId}").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/complain-comments/list/{id}").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/complain-comments/comment/{complainId}").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/news/news/{newsId}").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/news/newsList").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/news/new/resident").hasAnyRole( "ADMIN")
                        .antMatchers("/v1/news/schedule").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/news/progress").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/news/completion").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/news-comments/resident/news/{newsId}").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/news-comments/list/{newsId}").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/visit-car/new/visitCar").hasAnyRole(  "USER")
                        .antMatchers("/v1/visit-car/list/visitCar").hasAnyRole(  "USER")
                        .antMatchers("/v1/visit-car/visitCar/{visitCarId}").hasAnyRole(  "USER")
                        .antMatchers("/v1/visit-car/update/visitCar/{visitCarId}").hasAnyRole(  "ADMIN")
                        .antMatchers("/v1/meeting/new/meeting/{category}").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting/list").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting/list/getHealth").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting/list/getResell").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting/list/getSoccer").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting/list/getMomCafe").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting/list/getFamousRestourant").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting/list/getBaseball").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting/list/getClimbing").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting/list/getClimbing/{category}").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting/{meetingId}").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting-comment/comment/{meetingId}").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/meeting-comment/getComment/{meetingId}").hasAnyRole(  "ADMIN","USER")
                        .antMatchers("/v1/resident/all").hasAnyRole(  "ADMIN")
                        .antMatchers("/v1/resident/{id}").hasAnyRole(  "ADMIN")




                        .anyRequest().hasRole("ADMIN") // 기본 접근 권한은 ROLE_ADMIN
                .and()
                    .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
                .and()
                    .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .and()
                    .addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOriginPattern("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
