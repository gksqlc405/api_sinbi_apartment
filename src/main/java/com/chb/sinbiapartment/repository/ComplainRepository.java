package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.enums.ServiceState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ComplainRepository extends JpaRepository<Complain, Long> {

    List<Complain> findAllByServiceStateOrderByComplainIdAsc(ServiceState serviceState);

}
