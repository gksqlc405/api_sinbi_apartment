package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.ComplainComments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ComplainCommentsRepository extends JpaRepository<ComplainComments, Long> {

    List<ComplainComments> findAllByComplain_ComplainIdOrderByIdAsc(long complainId);
}
