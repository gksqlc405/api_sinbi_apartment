package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.News;
import com.chb.sinbiapartment.enums.NewsState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NewsRepository extends JpaRepository<News, Long> {
    List<News> findAllByNewsStateOrderByNewsIdAsc(NewsState newsState);
}
