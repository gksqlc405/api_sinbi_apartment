package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.AdminCost;
import com.chb.sinbiapartment.entity.Resident;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;

public interface AdminCostRepository extends JpaRepository<AdminCost, Long> {

    AdminCost findAllByResident_ResidentIdAndDateStartGreaterThanEqualAndDateStartLessThanEqual(long residentId, LocalDate dateStart, LocalDate dateEnd);
}


