package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.Resident;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ResidentRepository extends JpaRepository<Resident, Long> {


    Optional<Resident> findByUsername(String username);

    long countByUsername(String username);
}
