package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.NewsComments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NewsCommentsRepository extends JpaRepository<NewsComments, Long> {

    List<NewsComments> findAllByNews_NewsIdOrderByIdAsc(long newsId);
}
