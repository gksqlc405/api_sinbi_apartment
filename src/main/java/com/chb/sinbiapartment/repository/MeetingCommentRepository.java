package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.MeetingComments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MeetingCommentRepository extends JpaRepository<MeetingComments, Long> {

    List<MeetingComments> findAllByMeeting_MeetingIdOrderByIdAsc(long meetingId);
}
