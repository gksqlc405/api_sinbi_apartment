package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.HouseMember;
import com.chb.sinbiapartment.entity.MyCar;
import com.chb.sinbiapartment.entity.Resident;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MyCarRepository extends JpaRepository<MyCar, Long> {

    List<MyCar> findAllByResident_ResidentId(long residentId);
}
