package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.Meeting;
import com.chb.sinbiapartment.enums.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MeetingRepository extends JpaRepository<Meeting, Long> {

    List<Meeting> findAllByCategory(Category category);
}
