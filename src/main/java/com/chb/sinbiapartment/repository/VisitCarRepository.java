package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.VisitCar;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface VisitCarRepository extends JpaRepository<VisitCar, Long> {

    List<VisitCar> findAllByResident_ResidentIdOrderByDateCreateAsc(long residentId);

}
