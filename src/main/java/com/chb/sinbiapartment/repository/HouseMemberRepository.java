package com.chb.sinbiapartment.repository;

import com.chb.sinbiapartment.entity.HouseMember;
import com.chb.sinbiapartment.entity.Resident;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HouseMemberRepository extends JpaRepository<HouseMember, Long> {

    List<HouseMember> findAllByResident_ResidentId(long residentId);
}
