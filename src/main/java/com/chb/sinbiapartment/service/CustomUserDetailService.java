package com.chb.sinbiapartment.service;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.exception.CNoUserNameException;
import com.chb.sinbiapartment.exception.CUsernameSignInFailedException;
import com.chb.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final ResidentRepository residentRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CUsernameSignInFailedException::new);
        return resident;
    }
}
