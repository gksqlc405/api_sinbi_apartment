package com.chb.sinbiapartment.service.manager;

import com.chb.sinbiapartment.entity.HouseMember;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.HouseMemberListItem;
import com.chb.sinbiapartment.model.HouseMemberRequest;
import com.chb.sinbiapartment.model.ListResult;
import com.chb.sinbiapartment.model.ProfileResponse;
import com.chb.sinbiapartment.repository.HouseMemberRepository;
import com.chb.sinbiapartment.repository.ResidentRepository;
import com.chb.sinbiapartment.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HouseMemberManagerService {
    private final HouseMemberRepository houseMemberRepository;
    private final ResidentRepository residentRepository;

    /**
     * 세대원 등록
     *
     * @param request 세대원 등록 정보들
     * @param resident 입주민 시퀀스
     */
    public void setHouseMember(HouseMemberRequest request, Resident resident) {
        HouseMember houseMember = new HouseMember.HouseMemberBuilder(request,resident).build();
        houseMemberRepository.save(houseMember);
    }

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }
//
//    public ProfileResponse getProfile() {
//        Resident resident = getMemberData();
//        return new ProfileResponse.ProfileResponseBuilder(resident).build();
//    }



    /**
     * 세대원 정보 가저오기
     *
     * @return  세대원 정보들
     */
    public ListResult<HouseMemberListItem> getHouseMemberList() {
        List<HouseMemberListItem> result = new LinkedList<>();
        Resident resident = getMemberData();

        List<HouseMember> houseMembers = houseMemberRepository.findAllByResident_ResidentId(resident.getResidentId());

        houseMembers.forEach(houseMember -> {
            HouseMemberListItem addItem = new HouseMemberListItem.HouseMemberDetailBuilder(houseMember).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
