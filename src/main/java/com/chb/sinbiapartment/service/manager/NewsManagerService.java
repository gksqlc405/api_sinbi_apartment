package com.chb.sinbiapartment.service.manager;

import com.chb.sinbiapartment.entity.News;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.NewsRequest;
import com.chb.sinbiapartment.repository.NewsRepository;
import com.chb.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NewsManagerService {
    private final NewsRepository newsRepository;
    private final ResidentRepository residentRepository;

    public News getNewsId(long newsId) {
        return newsRepository.findById(newsId).orElseThrow();
    }

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 아파트소식 등록
     *
     * @param request 아파트소식 등록 정보들
     */
    public void setNews(NewsRequest request) {
        Resident resident = getMemberData();
        News news = new News.NewsBuilder(request, resident).build();
        newsRepository.save(news);
    }
}
