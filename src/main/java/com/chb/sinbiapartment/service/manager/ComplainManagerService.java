package com.chb.sinbiapartment.service.manager;

import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.enums.ServiceState;
import com.chb.sinbiapartment.repository.ComplainRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ComplainManagerService {
    private final ComplainRepository complainRepository;

    /**
     * 민원창구 진행상태 수정
     *
     * @param complainId
     * @param serviceState
     */
    public void putComplainState(long complainId, ServiceState serviceState) {
        Complain complain = complainRepository.findById(complainId).orElseThrow();
        complain.putComplainState(serviceState);
        complainRepository.save(complain);
    }
}
