package com.chb.sinbiapartment.service.manager;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.enums.ResidentGroup;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.exception.CUserNameException;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.repository.ResidentRepository;
import com.chb.sinbiapartment.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ResidentManagerService {
    private final ResidentRepository residentRepository;

    /**
     * 입주민 시퀀스 가저오기
     *
     * @param residentId 입주민 시퀀스
     * @return 입주민 테이블
     */
    public Resident getResidentId(long residentId) {
      return residentRepository.findById(residentId).orElseThrow();

    }


    public ListResult<ResidentList> residentListItem() {
        List<ResidentList> result = new LinkedList<>();

        List<Resident> residents = residentRepository.findAll();

        residents.forEach(resident -> {
            ResidentList addItem = new ResidentList.ResidentListBuilder(resident).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    public ResidentDetailItem getResidentDetails(long residentId) {
        Resident resident = residentRepository.findById(residentId).orElseThrow();

        return new ResidentDetailItem.ResidentDetailItemBuilder(resident).build();

    }

}
