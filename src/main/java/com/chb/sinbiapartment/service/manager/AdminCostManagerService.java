package com.chb.sinbiapartment.service.manager;

import com.chb.sinbiapartment.entity.AdminCost;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.AdminCostDetail;
import com.chb.sinbiapartment.model.AdminCostRequest;
import com.chb.sinbiapartment.repository.AdminCostRepository;
import com.chb.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Calendar;

@Service
@RequiredArgsConstructor
public class AdminCostManagerService {
    private final AdminCostRepository adminCostRepository;
    private final ResidentRepository residentRepository;

    /**
     * 관리비 등록
     *
     * @param request 등록할 관리비
     * @param resident 입주민 시퀀스
     */
    public void setAdminCost(AdminCostRequest request, Resident resident) {
        AdminCost adminCost = new AdminCost.AdminCostBuilder(request, resident).build();

        adminCostRepository.save(adminCost);
    }

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }


    public AdminCostDetail getAdminCostDetail(int year, int month ) {


        LocalDate dateStart = LocalDate.of(year, month ,1);

        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1 ,1 );
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        LocalDate dateEnd = LocalDate.of(year, month, maxDay);

        Resident resident = getMemberData();

       AdminCost adminCost = adminCostRepository.findAllByResident_ResidentIdAndDateStartGreaterThanEqualAndDateStartLessThanEqual(resident.getResidentId() ,dateStart, dateEnd);
       return new AdminCostDetail.AdminCostDetailBuilder(adminCost).build();
    }

}


