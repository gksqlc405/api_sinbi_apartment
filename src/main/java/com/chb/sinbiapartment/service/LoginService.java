package com.chb.sinbiapartment.service;

import com.chb.sinbiapartment.configure.JwtTokenProvider;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.enums.ResidentGroup;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.exception.CNoUserNameException;
import com.chb.sinbiapartment.model.LoginRequest;
import com.chb.sinbiapartment.model.LoginResponse;
import com.chb.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final ResidentRepository residentRepository;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    // 로그인 타입은 WEB or APP (WEB인경우 토큰 유효시간 10시간, APP은 1년)
    public LoginResponse doLogin(ResidentGroup residentGroup, LoginRequest loginRequest, String loginType) {
        Resident resident = residentRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CNoUserNameException::new); // 회원정보가 없습니다 던지기

        if (!resident.getIsEnable()) throw new CMissingDataException(); // 회원탈퇴인 경우인데 DB에 남아있으므로 이걸 안 들키려면 회원정보가 없습니다 이걸로 던져야 함.
        if (!resident.getResidentGroup().equals(residentGroup)) throw new CMissingDataException(); // 일반회원이 최고관리자용으로 로그인하려거나 이런 경우이므로 메세지는 회원정보가 없습니다로 일단 던짐.
        if (!passwordEncoder.matches(loginRequest.getPassword(), resident.getPassword())) throw new CMissingDataException(); // 비밀번호가 일치하지 않습니다 던짐

        String token = jwtTokenProvider.createToken(String.valueOf(resident.getUsername()), resident.getResidentGroup().toString(), loginType);

        return new LoginResponse.LoginResponseBuilder(token, resident.getResidentName()).build();
    }
}
