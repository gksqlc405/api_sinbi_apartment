package com.chb.sinbiapartment.service.Common;

import com.chb.sinbiapartment.entity.*;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.repository.MeetingCommentRepository;
import com.chb.sinbiapartment.repository.ResidentRepository;
import com.chb.sinbiapartment.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MeetingCommentsService {
    private final MeetingCommentRepository meetingCommentRepository;
    private final ResidentRepository residentRepository;


    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 입주민 카테고리 게시글 댓글
     *
     * @param request 댓글 내용
     */
    public void setMeetingComment(MeetingCommentRequest request, Meeting meeting) {
        Resident resident = getMemberData();
        MeetingComments meetingComments = new MeetingComments.MeetingCommentsBuilder(request, resident, meeting).build();
        meetingCommentRepository.save(meetingComments);
    }


    public ListResult<MeetingCommentListItem> getMeetingComment(long meetingId) {
        List<MeetingCommentListItem> result = new LinkedList<>();

        List<MeetingComments> meetingComments = meetingCommentRepository.findAllByMeeting_MeetingIdOrderByIdAsc(meetingId);

        meetingComments.forEach(meetingComments1 -> {
            MeetingCommentListItem addItem = new MeetingCommentListItem.MeetingCommentListItemBuilder(meetingComments1).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
