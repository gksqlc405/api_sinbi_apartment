package com.chb.sinbiapartment.service.Common;

import com.chb.sinbiapartment.entity.ComplainComments;
import com.chb.sinbiapartment.entity.News;
import com.chb.sinbiapartment.entity.NewsComments;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.ComplainCommentsListItem;
import com.chb.sinbiapartment.model.ListResult;
import com.chb.sinbiapartment.model.NewsCommentListItem;
import com.chb.sinbiapartment.model.NewsCommentsRequest;
import com.chb.sinbiapartment.repository.NewsCommentsRepository;
import com.chb.sinbiapartment.repository.ResidentRepository;
import com.chb.sinbiapartment.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NewsCommentsService{
    private final NewsCommentsRepository newsCommentsRepository;
    private final ResidentRepository residentRepository;



    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 아파트 소식 댓글
     *
     * @param request 댓글 내용
     * @param news 아파트 소식 시퀀스
     */
    public void setNewsComments(NewsCommentsRequest request, News news) {
        Resident resident = getMemberData();
        NewsComments newsComments = new NewsComments.NewsCommentsBuilder(request, resident, news).build();
        newsCommentsRepository.save(newsComments);
    }


    /**
     *  아파트소식 댓글 리스트 가저오기
     *
     * @return 아파트소식 댓글 리스트 정보들
     */
    public ListResult<NewsCommentListItem> getNewsCommentList(long newsId) {
        List<NewsCommentListItem> result = new LinkedList<>();

        List<NewsComments> newsComments = newsCommentsRepository.findAllByNews_NewsIdOrderByIdAsc(newsId);

        newsComments.forEach(newsComments1 -> {
            NewsCommentListItem addItem = new NewsCommentListItem.NewsCommentListItemBuilder(newsComments1).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
