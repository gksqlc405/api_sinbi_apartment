package com.chb.sinbiapartment.service.Common;

import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.entity.ComplainComments;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.ComplainCommentRequest;
import com.chb.sinbiapartment.model.ComplainCommentsListItem;
import com.chb.sinbiapartment.model.ComplainTitleList;
import com.chb.sinbiapartment.model.ListResult;
import com.chb.sinbiapartment.repository.ComplainCommentsRepository;
import com.chb.sinbiapartment.repository.ResidentRepository;
import com.chb.sinbiapartment.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ComplainCommentsService {
    private final ComplainCommentsRepository complainCommentsRepository;
    private final ResidentRepository residentRepository;


    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 민원창구 댓글
     *
     * @param request 민원창구 댓글 내용
     * @param complain 민원창구 시퀀
     */
    public void setComplainComments(ComplainCommentRequest request,Complain complain) {
        Resident resident = getMemberData();

        ComplainComments complainComments = new ComplainComments.ComplainCommentsBuilder(request,resident, complain).build();
        complainCommentsRepository.save(complainComments);
    }

    /**
     * 민원 창구 댓글 리스트 가저오기
     *
     * @return 민원 창구 댓글 리스트 정보들
     */
    public ListResult<ComplainCommentsListItem> getCommentList(long complainId) {
        List<ComplainCommentsListItem> result = new LinkedList<>();

        List<ComplainComments> complainComments = complainCommentsRepository.findAllByComplain_ComplainIdOrderByIdAsc(complainId);

        complainComments.forEach(complainComment -> {
            ComplainCommentsListItem addItem = new ComplainCommentsListItem.ComplainCommentsListItemBuilder(complainComment).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
