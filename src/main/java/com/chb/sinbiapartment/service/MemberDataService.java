package com.chb.sinbiapartment.service;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.enums.ResidentGroup;
import com.chb.sinbiapartment.exception.CNoPasswordException;
import com.chb.sinbiapartment.exception.CUserNameException;
import com.chb.sinbiapartment.exception.CValidUsernameException;
import com.chb.sinbiapartment.lib.CommonCheck;
import com.chb.sinbiapartment.model.ResidentRequest;
import com.chb.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final ResidentRepository residentRepository;
    private final PasswordEncoder passwordEncoder;

    public void setFirstMember() {
        String username = "gksqlc405";
        String password = "gksqlc12";
        boolean isSuperAdmin = isNewUsername(username);


        if (isSuperAdmin) {
            ResidentRequest createRequest = new ResidentRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setResidentName("최고관리자");
            createRequest.setAddress("----");
            createRequest.setAptDong(0);
            createRequest.setAptNumber(0);
            createRequest.setPhone("010-0000-0000");

            setMember(ResidentGroup.ROLE_ADMIN, createRequest);
        }
    }

    public void setMember(ResidentGroup residentGroup, ResidentRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CValidUsernameException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CNoPasswordException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CUserNameException(); // 중복된 아이디가 존재합니다 던지기

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        Resident resident = new Resident.ResidentBuilder(residentGroup, createRequest).build();
        residentRepository.save(resident);
    }

    private boolean isNewUsername(String username) {
        long dupCount = residentRepository.countByUsername(username);
        return dupCount <= 0;
    }
}
