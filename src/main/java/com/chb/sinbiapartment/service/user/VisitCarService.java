package com.chb.sinbiapartment.service.user;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.entity.VisitCar;
import com.chb.sinbiapartment.enums.ApprovalStatus;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.ListResult;
import com.chb.sinbiapartment.model.VisitCarApprovalStatusUpdateRequest;
import com.chb.sinbiapartment.model.VisitCarListItem;
import com.chb.sinbiapartment.model.VisitCarRequest;
import com.chb.sinbiapartment.repository.ResidentRepository;
import com.chb.sinbiapartment.repository.VisitCarRepository;
import com.chb.sinbiapartment.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VisitCarService {
    private final VisitCarRepository visitCarRepository;
    private final ResidentRepository residentRepository;


    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 방문차량 등록
     *
     * @param request 방문차량 번호
     */
    public void setVisitCar(VisitCarRequest request) {
        Resident resident =getMemberData();
        VisitCar visitCar = new VisitCar.VisitCarBuilder(request,resident).build();
        visitCarRepository.save(visitCar);
    }




    /**
     * 방문차량 신청내역 조회
     *

     * @return 조회할 내역 정보들
     */
    public ListResult<VisitCarListItem> getVisitCarList() {
        List<VisitCarListItem> result = new LinkedList<>();


        Resident resident = getMemberData();

        List<VisitCar> visitCars = visitCarRepository.findAllByResident_ResidentIdOrderByDateCreateAsc(resident.getResidentId());
        visitCars.forEach(visitCar -> {
            VisitCarListItem addItem = new VisitCarListItem.VisitCarListItemBuilder(visitCar).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 방문차량 신청상태 수정
     *
     * @param visitCarId 입주민 시퀀스
     * @param isApproval 승인여부
     */
    public void putVisitCar(long visitCarId, boolean isApproval) {
        VisitCar visitCar = visitCarRepository.findById(visitCarId).orElseThrow();

        if (isApproval) visitCar.putProcessed();
        else visitCar.putApprovalStatus();
        visitCarRepository.save(visitCar);

    }

    public void delVisitCar(long visitCar) {
        visitCarRepository.deleteById(visitCar);
    }

}
