package com.chb.sinbiapartment.service.user;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.ResidentDetail;
import com.chb.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ResidentService {
    private final ResidentRepository residentRepository;


    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }
//
//    public ResidentDetail getResidentDetail() {
//        Resident resident = getMemberData();
//        return new ResidentDetail.ResidentDetailBuilder(resident).build();
//    }

    /**
     * 입주민 정보 디테일
     *
     * @return 입주민 정보들
     */
    public ResidentDetail getResidentDetail() {
        Resident resident = getMemberData();
        return new ResidentDetail.ResidentDetailBuilder(resident).build();
    }


}
