package com.chb.sinbiapartment.service.user;

import com.chb.sinbiapartment.entity.News;
import com.chb.sinbiapartment.enums.NewsState;
import com.chb.sinbiapartment.model.ListResult;
import com.chb.sinbiapartment.model.NewsDetail;
import com.chb.sinbiapartment.model.NewsTitleListItem;
import com.chb.sinbiapartment.repository.NewsRepository;
import com.chb.sinbiapartment.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NewsService {
    private final NewsRepository newsRepository;


    /**
     * 아파트 소식 리스트 가저오기
     *
     * @return 아파트소식 리스트에 필요한 제목과 진행상태
     */
    public ListResult<NewsTitleListItem> getNewsTitleList() {
        List<NewsTitleListItem> result = new LinkedList<>();

        List<News> news = newsRepository.findAll();

        news.forEach(news1 -> {
            NewsTitleListItem addItem = new NewsTitleListItem.NewsTitleListItemBuilder(news1).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 아파트 소식 예정리스트 가저오기
     *
     * @return 아파트소식 리스트에 필요한 제목과 진행상태
     */
    public ListResult<NewsTitleListItem> getScheduleList() {
        List<NewsTitleListItem> result = new LinkedList<>();

        NewsState schedule = NewsState.SCHEDULE;

        List<News> news = newsRepository.findAllByNewsStateOrderByNewsIdAsc(schedule);

        news.forEach(news1 -> {
            NewsTitleListItem addItem = new NewsTitleListItem.NewsTitleListItemBuilder(news1).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 아파트 소식 진행리스트 가저오기
     *
     * @return 아파트소식 리스트에 필요한 제목과 진행상태
     */
    public ListResult<NewsTitleListItem> getProgress() {
        List<NewsTitleListItem> result = new LinkedList<>();

        NewsState progress = NewsState.PROGRESS;

        List<News> news = newsRepository.findAllByNewsStateOrderByNewsIdAsc(progress);

        news.forEach(news1 -> {
            NewsTitleListItem addItem = new NewsTitleListItem.NewsTitleListItemBuilder(news1).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 아파트 소식 완료 리스트 가저오기
     *
     * @return 아파트소식 리스트에 필요한 제목과 진행상태
     */
    public ListResult<NewsTitleListItem> getCompletion() {
        List<NewsTitleListItem> result = new LinkedList<>();

        NewsState completion = NewsState.COMPLETION;

        List<News> news = newsRepository.findAllByNewsStateOrderByNewsIdAsc(completion);

        news.forEach(news1 -> {
            NewsTitleListItem addItem = new NewsTitleListItem.NewsTitleListItemBuilder(news1).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 아파트 소식 진행상태 수정
     *
     * @param newsId 아파트 소식 시퀀스
     * @param newsState 진행상태
     */
    public void putNewsState(long newsId, NewsState newsState ) {
        News news = newsRepository.findById(newsId).orElseThrow();
        news.putNewsState(newsState);
        newsRepository.save(news);
    }

    public NewsDetail getNewsDetail(long newsId) {
        News news = newsRepository.findById(newsId).orElseThrow();
        return new NewsDetail.NewsDetailBuilder(news).build();
    }
}
