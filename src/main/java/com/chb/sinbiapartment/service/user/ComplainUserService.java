package com.chb.sinbiapartment.service.user;

import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.enums.ServiceState;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.repository.ComplainRepository;
import com.chb.sinbiapartment.repository.ResidentRepository;
import com.chb.sinbiapartment.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ComplainUserService {
    private final ComplainRepository complainRepository;
    private final ResidentRepository residentRepository;

    /**
     * 민원창구 시퀀스 가저오기
     *
     * @param complainId 민원창구 시퀀스
     * @return 민원창구 테이블
     */
    public Complain getComplainId(long complainId) {
        return complainRepository.findById(complainId).orElseThrow();
    }

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 민원창구
     *
     * @param request 민원창구 쓸 내용
     */
    public void setComplain(ComplainRequest request) {
        Resident resident = getMemberData();
        Complain complain = new Complain.ComplainBuilder(request, resident).build();
        complainRepository.save(complain);
    }

    /**
     * 민원 창구 리스트 가저오기
     *
     * @return 민원 창구 리스트 정보들
     */
    public ListResult<ComplainTitleList> getComplainList() {
        List<ComplainTitleList> result = new LinkedList<>();

        List<Complain> complains = complainRepository.findAll();

        complains.forEach(complain -> {
            ComplainTitleList addItem = new ComplainTitleList.ComplainTitleListBuilder(complain).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }


    /**
     * 선택한 민원창구 디테일 가저오기
     *
     * @param complainId  민원창구 시퀀스
     * @return 민원창구 디테일에 들어간 정보들
     */
    public ComplainDetail getComplainDetail(long complainId) {
        Complain complain = complainRepository.findById(complainId).orElseThrow();
        return new ComplainDetail.ComplainDetailBuilder(complain).build();
    }

    /**
     * 민원 창구 접수리스트 가저오기
     *
     * @return 민원 창구 접수리스트 정보들
     */
    public ListResult<ComplainStateListItem> getRegisterList() {
        ServiceState example = ServiceState.REGISTER;

        List<ComplainStateListItem> result = new LinkedList<>();

        List<Complain> complains = complainRepository.findAllByServiceStateOrderByComplainIdAsc(example);

        complains.forEach(complain -> {
            ComplainStateListItem addItem = new ComplainStateListItem.ComplainRegisterListItemBuilder(complain).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 민원 창구 진행리스트 가저오기
     *
     * @return 민원 창구 진행리스트 정보들
     */
    public ListResult<ComplainStateListItem> getProgressList() {
        ServiceState example = ServiceState.PROGRESS;

        List<ComplainStateListItem> result = new LinkedList<>();

        List<Complain> complains = complainRepository.findAllByServiceStateOrderByComplainIdAsc(example);

        complains.forEach(complain -> {
            ComplainStateListItem addItem = new ComplainStateListItem.ComplainRegisterListItemBuilder(complain).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 민원 창구 완료리스트 가저오기
     *
     * @return 민원 창구 완료리스트 정보들
     */
    public ListResult<ComplainStateListItem> getCompletion() {
        ServiceState example = ServiceState.COMPLETION;

        List<ComplainStateListItem> result = new LinkedList<>();

        List<Complain> complains = complainRepository.findAllByServiceStateOrderByComplainIdAsc(example);

        complains.forEach(complain -> {
            ComplainStateListItem addItem = new ComplainStateListItem.ComplainRegisterListItemBuilder(complain).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

}
