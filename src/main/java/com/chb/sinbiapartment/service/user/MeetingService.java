package com.chb.sinbiapartment.service.user;

import com.chb.sinbiapartment.entity.Complain;
import com.chb.sinbiapartment.entity.Meeting;
import com.chb.sinbiapartment.entity.News;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.enums.Category;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.*;
import com.chb.sinbiapartment.repository.MeetingRepository;
import com.chb.sinbiapartment.repository.ResidentRepository;
import com.chb.sinbiapartment.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.xml.catalog.Catalog;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MeetingService {
    private final MeetingRepository meetingRepository;
    private final ResidentRepository residentRepository;


    public Meeting getMeetingId(long meetingId) {
        return meetingRepository.findById(meetingId).orElseThrow();
    }

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    public void setMeeting(Category category, MeetingRequest request) {
        Resident resident = getMemberData();
        Meeting meeting = new Meeting.MeetingBuilder(resident, category, request).build();
        meetingRepository.save(meeting);
    }


    public ListResult<MeetingListItem> getMeetingList() {
        List<MeetingListItem> result = new LinkedList<>();



        for (Category category : Category.values()) {
            result.add(new MeetingListItem.MeetingListItemBuilder(category).build());
        }
        return ListConvertService.settingResult(result);
    }

    public ListResult<MeetingBulletinListItem> getCategories(Category category) {
        List<MeetingBulletinListItem> result = new LinkedList<>();

        List<Meeting> meetings = meetingRepository.findAllByCategory(category);
        meetings.forEach(meeting -> {
            MeetingBulletinListItem addItem = new MeetingBulletinListItem.MeetingBulletinListItemBuilder(meeting).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);

    }

    public MeetingBulletinDetails getBulletinDetail(long meetingId) {
        Meeting meeting = meetingRepository.findById(meetingId).orElseThrow();
        return new MeetingBulletinDetails.MeetingBulletinDetailsBuilder(meeting).build();
    }

}
