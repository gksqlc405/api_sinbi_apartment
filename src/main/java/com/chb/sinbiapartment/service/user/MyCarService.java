package com.chb.sinbiapartment.service.user;

import com.chb.sinbiapartment.entity.MyCar;
import com.chb.sinbiapartment.entity.Resident;
import com.chb.sinbiapartment.exception.CAccessDeniedException;
import com.chb.sinbiapartment.exception.CMissingDataException;
import com.chb.sinbiapartment.model.ListResult;
import com.chb.sinbiapartment.model.MyCarListItem;
import com.chb.sinbiapartment.model.MyCarRequest;
import com.chb.sinbiapartment.repository.MyCarRepository;
import com.chb.sinbiapartment.repository.ResidentRepository;
import com.chb.sinbiapartment.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MyCarService {
    private final MyCarRepository myCarRepository;
    private final ResidentRepository residentRepository;


    /**
     * 우리집 차량 등록
     *
     * @param request 등록할 차량 정보들
     * @param resident 입주민 시퀀스
     */
    public void setMyCar(MyCarRequest request, Resident resident) {
        MyCar myCar = new MyCar.MyCarBuilder(request,resident).build();
        myCarRepository.save(myCar);
    }

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }


    public ListResult<MyCarListItem> getMyCarList() {
        List<MyCarListItem> result = new LinkedList<>();

        Resident resident = getMemberData();

        List<MyCar> myCars = myCarRepository.findAllByResident_ResidentId(resident.getResidentId());

        myCars.forEach(myCar -> {
            MyCarListItem addItem = new  MyCarListItem.MyCarListItemBuilder(myCar).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }
}
