# 아파트 관리  API

***

#### LANGUAGE
```
JAVA 14
SpringBoot 2.7.3
```
#### 기능
```

* 로그인 관리
  - Security
  - 사용자 로그인(token) 
  - 관리자 로그인(token) 
  
* 관리비
  - 월별내역서 가저오기 (Get)
  - 관리비 등록 (Post)
  
* 민원 창구
  - 민원창구 상세페이지 가저오기 (Get)
  - 민원창구 완료 리스트 가저오기(Get)
  - 민원창구 진행 리스트 가저오기(Get)
  - 민원창구 접수 리스트 가저오기(Get)
  - 민원창구 모든민원 리스트 가저오기(Get)
  - 민원창구 게시글 등록 (Post)
  - 민원창구 댓글 리스트 가저오기 (Get)
  - 민원창구 댓글 등록 (Post)
  - 민원 진행상태 수정하기 (Put)

* 방문 차량 (사용자)
  - 방문차량 신청내역 조회 (Get)
  - 방문차량 등록 (Post)
  - 방문차량 승인 (Put)
  - 방문차량 신청 취소 (Del)
  
* 세대원 (사용자)
  - 세대원 정보 가저오기 (Get)
  - 세대원 등록 (Post)
  
* 아파트 소식 (사용자)
  - 아파트 소식 완료 리스트 가저오기 (Get)
  - 아파트 소식 진행 리스트 가저오기 (Get)
  - 아파트 소식 예정 리스트 가저오기 (Get)
  - 아파트 소식 상세정보 가저오기 (Get)
  - 아파트 소식 전체 리스트 가저오기 (Get)
  - 아파트 소식 댓글 리스트 가저오기 (Get)
  - 아파트 소식 댓글 등록 (Post)
  - 아파트 소식 게시글 등록 (Post)
  - 아파트 소식 진행상태 수정 (Put)
  
 
* 입주민 라운지 (사용자)
  - 카테고리 게시글 상세페이지 가저오기 (Get)
  - 카테고리 리스트 가저오기 (Get)
  - 카테고리 게시글 리스트 가저오기 (Get)
  - 카테고리 게시글 등록 (Post)
  - 카테고리 게시글 댓글 등록 (Post) 
  - 카테고리 게시글 댓글 리스트 가저오기 (Get)
 
* 입주민 차량 
  - 입주민 차량 리스트 가저오기 (Get)
  - 입주민 차량 등록 (Post)
  
* 입주민 관리
  - 입주민 상세정보 가저오기 (Get)
  - 입주민 정보 등록 (Post)

```


 * Swagger 전체
![SwaggerList](./images/all.png)

* Swagger 
  ![SwaggerList](./images/swagger1.png)

* Swagger 
  ![SwaggerList](./images/swagger2.png)
* 
* Swagger 
  ![SwaggerList](./images/swagger3.png)
* 
* Swagger 
  ![SwaggerList](./images/swagger4.png)
* 
* Swagger 
  ![SwaggerList](./images/swagger5.png)
* 
* Swagger 
  ![SwaggerList](./images/swagger6.png)
* 
* Swagger 
  ![SwaggerList](./images/swagger7.png)
* 
* Swagger 
  ![SwaggerList](./images/swagger8.png)
* 
* Swagger 
  ![SwaggerList](./images/swagger9.png)
* 
* Swagger 
  ![SwaggerList](./images/swagger10.png)
* 
* Swagger 
  ![SwaggerList](./images/swagger11.png)
* 
* Swagger 
  ![SwaggerList](./images/swagger12.png)



